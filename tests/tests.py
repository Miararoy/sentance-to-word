from src.sentence_to_words import Dictionary, break_sentence


def test_word_search():
    d = Dictionary(
        {
            "a": 1,
            "b": 2
        }
    )
    assert d.is_a_word("a")
    assert not d.is_a_word("c")


def test_break_sentence():
    dictionary = {
        "a": 1,
        "b": 2
    }
    good_sentence = "aabb"
    bad_sentence = "abc"
    wrong_type_sentence = 123
    assert break_sentence(good_sentence, dictionary) == ["a a b b"]
    assert break_sentence(bad_sentence, dictionary) == []
    assert break_sentence(wrong_type_sentence, dictionary) == []
