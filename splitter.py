from utils import get_word_by_priority
import argparse

word_dict = {
    "the": 5,
    "is": 4,
    "and": 4,
    "dog": 2,
    "cat": 1,
    "ate": 1,
    "do": 3,
    "gate": 2,
    "drink": 1,
    "watch": 1,
}
DICTIONARY = get_word_by_priority(word_dict)

parser = argparse.ArgumentParser()
parser.add_argument(
    "--sentance",
    "-s", 
    help="The sentace to split", 
    default="thedogatethecat"
)


def break_sentance(
    sentance,
    broken
):
    if sentance is "": print (broken)
    return [
        break_sentance(sentance[len(word):], broken + [word]) 
        for word in [w for w in DICTIONARY if sentance.startswith(w)]
    ]


def main():
    args = parser.parse_args()
    break_sentance(args.sentance, [])


if __name__ == "__main__":
    main()