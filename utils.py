import operator
import sys

def get_word_by_priority(dictionary):
    if sys.version_info[0] < 3:
        sorted_tuple_dict = sorted(
            dictionary.iteritems(),
            key=operator.itemgetter(1),
            reverse=True
        )
    else:
        sorted_tuple_dict = sorted(
            dictionary.items(),
            key=operator.itemgetter(1),
            reverse=True
        )
    return [w[0] for w in sorted_tuple_dict]