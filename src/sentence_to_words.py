# pylint: disable=E0401

import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--sentence",
    "-s",
    help="The sentence to split",
    default="thedogatethecat"
)


class Dictionary(object):
    """
    A simple implementation of a dictionary with a constant word bank
    args:
        DEFAULT_DICT(dict): a default dictionary of words
    """
    DEFAULT_DICT = {
        "the": "",
        "is": "",
        "and": "",
        "dog": "",
        "cat": "",
        "ate": "",
        "do": "",
        "gate": "",
        "drink": "",
        "watch": "",
    }

    def __init__(
        self,
        word_bank=DEFAULT_DICT
    ):
        """
        initializes dictionary with a given word bank
        args:
            word_bank(dict): a dictionary describing all the words in
                             this dictionary
        """
        self._word_bank = word_bank

    def is_a_word(
        self,
        word
    ):
        """
        searches if a word exists in a dictionary
        args:
            word(str): the word to search
        return
            b(bool): True if word exists in dictionary, else False
        """
        return word in self._word_bank


def break_sentence(sentence, dictionary=None):
    """
    this function breaks a sentence to words, printing out the
    broken word array
    args:
        sentence(str): a spaceless sentence
    returns:
        possible_sentences(list): list of all possible words
    """
    possible_sentences = []

    if dictionary is None:
        _d = Dictionary()
    else:
        _d = Dictionary(dictionary)
    
    if not isinstance(sentence, str):
        return possible_sentences
    
    def _break_sentence_rec(
        sent,
        index,
        broken
    ):  
        """
        recursive function that breaks spaceless sentence to words
        args:
            sent(str): the remaining sentence to break
            index(int): index determining how deep indide we're
                        breaking the begining of sent
            broken(list): an append list to which broken words are added

        """
        if sent == "":
            possible_sentences.append(" ".join(broken))
            return
        if index > len(sent):
            return
        word_candidate = sent[:index]
        if _d.is_a_word(word_candidate):
            _break_sentence_rec(
                sent[index:],
                0,
                broken + [word_candidate]
            )
            _break_sentence_rec(
                sent,
                index + 1,
                broken
            )
        else:
            _break_sentence_rec(
                sent,
                index + 1,
                broken
            )
    _break_sentence_rec(sentence, 0, [])
    return possible_sentences


def main():
    args = parser.parse_args()
    print(break_sentence(args.sentence))


if __name__ == "__main__":
    main()
