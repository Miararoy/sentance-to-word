# Sentance to word splitter

Takes a long, spaceless sentance and breaks it into words.

## to run

1. ```git clone https://gitlab.com/Miararoy/sentance-to-word.git```
2. ```cd sentance-to-word/```
3. run tests
    - ```python3 -m pytest ./tests/tests.py```
4. run script
    - ```python3 src/sentence_to_words.py -s <SENTANCE TO SPLIT>```

NOTE: Words can be only from the following set: 
{
    "the",
    "is", 
    "and",
    "dog",
    "cat",
    "ate",
    "do", 
    "gate",
    "drink",
    "watch"
}

## running example 

running 

```python3 src/sentence_to_words.py -s thedogandthedogatethecat``` 


should print 
```
['the dog and the do gate the cat', 'the dog and the dog ate the cat']
```